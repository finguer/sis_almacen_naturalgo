<?php
/**
*@package pXP
*@file gen-MODFichaDetalle.php
*@author  (admin)
*@date 24-04-2015 18:12:03
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODFichaDetalle extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarFichaDetalle(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='prod.ft_ficha_detalle_sel';
		$this->transaccion='PROD_FICDE_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_ficha_detalle','int4');
		$this->captura('puntos_comisionable','numeric');
		$this->captura('estado_reg','varchar');
		$this->captura('puntos_pagar','numeric');
		$this->captura('puntos_volumen','numeric');
		$this->captura('cantidad','int4');
		$this->captura('precio_afiliado','numeric');
		$this->captura('id_producto','int4');
		$this->captura('puntos_obtenidos','numeric');
		$this->captura('id_ficha','int4');
		$this->captura('usuario_ai','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('id_usuario_reg','int4');
		$this->captura('id_usuario_ai','int4');
		$this->captura('id_usuario_mod','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');

		$this->captura('codigo','varchar');
		$this->captura('descripcion','varchar');

		$this->captura('precio_total','numeric');
		
		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarFichaDetalle(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_ficha_detalle_ime';
		$this->transaccion='PROD_FICDE_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('puntos_comisionable','puntos_comisionable','numeric');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('puntos_pagar','puntos_pagar','numeric');
		$this->setParametro('puntos_volumen','puntos_volumen','numeric');
		$this->setParametro('cantidad','cantidad','int4');
		$this->setParametro('precio_afiliado','precio_afiliado','numeric');
		$this->setParametro('id_producto','id_producto','varchar');
		$this->setParametro('puntos_obtenidos','puntos_obtenidos','numeric');
		$this->setParametro('id_ficha','id_ficha','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarFichaDetalle(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_ficha_detalle_ime';
		$this->transaccion='PROD_FICDE_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_ficha_detalle','id_ficha_detalle','int4');
		$this->setParametro('puntos_comisionable','puntos_comisionable','numeric');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('puntos_pagar','puntos_pagar','numeric');
		$this->setParametro('puntos_volumen','puntos_volumen','numeric');
		$this->setParametro('cantidad','cantidad','int4');
		$this->setParametro('precio_afiliado','precio_afiliado','numeric');
		$this->setParametro('id_producto','id_producto','varchar');
		$this->setParametro('puntos_obtenidos','puntos_obtenidos','numeric');
		$this->setParametro('id_ficha','id_ficha','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarFichaDetalle(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_ficha_detalle_ime';
		$this->transaccion='PROD_FICDE_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_ficha_detalle','id_ficha_detalle','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>