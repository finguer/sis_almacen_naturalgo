<?php
/**
*@package pXP
*@file gen-MODFicha.php
*@author  (admin)
*@date 24-04-2015 18:05:16
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODFicha extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarFicha(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='prod.ft_ficha_sel';
		$this->transaccion='PROD_FICH_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_ficha','int4');
		$this->captura('fecha','date');
		$this->captura('id_afiliado','int4');
		$this->captura('estado_reg','varchar');
		$this->captura('codigo','varchar');
		$this->captura('id_usuario_ai','int4');
		$this->captura('usuario_ai','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('id_usuario_reg','int4');
		$this->captura('id_usuario_mod','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		$this->captura('desc_person','text');
		$this->captura('ci','varchar');
		$this->captura('codigo_afiliado','numeric');
		$this->captura('nombre_sucursal','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarFicha(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_ficha_ime';
		$this->transaccion='PROD_FICH_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('fecha','fecha','date');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('codigo','codigo','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarFicha(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_ficha_ime';
		$this->transaccion='PROD_FICH_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_ficha','id_ficha','int4');
		$this->setParametro('fecha','fecha','date');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('codigo','codigo','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarFicha(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_ficha_ime';
		$this->transaccion='PROD_FICH_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_ficha','id_ficha','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}




	function insertarFichaCompleta(){




		//Abre conexion con PDO
		$cone = new conexion();
		$link = $cone->conectarpdo();
		$copiado = false;
		try {
			$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$link->beginTransaction();

			/////////////////////////
			//  inserta cabecera de la solicitud de compra
			///////////////////////

			//Definicion de variables para ejecucion del procedimiento
			$this->procedimiento = 'prod.ft_ficha_ime';
			$this->transaccion = 'PROD_FICH_INS';
			$this->tipo_procedimiento = 'IME';

			//Define los parametros para la funcion

			$id_afiliado = $id_patrocinador = $this->objParam->getParametro('id_afiliado');
			$id_sucursal = $id_patrocinador = $this->objParam->getParametro('id_sucursal');
			$fecha = $id_patrocinador = $this->objParam->getParametro('fecha');

			$this->setParametro('id_afiliado','id_afiliado','int4');
			$this->setParametro('id_sucursal','id_sucursal','int4');
			$this->setParametro('fecha','fecha','date');


			//Ejecuta la instruccion
			$this->armarConsulta();
			$stmt = $link->prepare($this->consulta);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			//recupera parametros devuelto depues de insertar ... (id_solicitud)
			$resp_procedimiento = $this->divRespuesta($result['f_intermediario_ime']);
			if ($resp_procedimiento['tipo_respuesta']=='ERROR') {
				throw new Exception("Error al ejecutar en la bd", 3);
			}

			$respuesta = $resp_procedimiento['datos'];

			$id_ficha = $respuesta['id_ficha'];

			//////////////////////////////////////////////
			//inserta detalle de la solicitud de compra
			/////////////////////////////////////////////



			//decodifica JSON  de detalles
			$json_detalle = $this->aParam->_json_decode($this->aParam->getParametro('json_new_records'));

			//var_dump($json_detalle)	;
			foreach($json_detalle as $f){

				$this->resetParametros();
				//Definicion de variables para ejecucion del procedimiento
				$this->procedimiento='prod.ft_ficha_detalle_ime';
				$this->transaccion='PROD_FICDE_INS';
				$this->tipo_procedimiento='IME';

				$total_puntos = $f['cantidad_pro'] * $f['puntos_volumen'];
				$total_pagar = $f['cantidad_pro'] * $f['puntos_comisionables'];
				$total = $f['cantidad_pro'] * $f['precio_afiliado'];
				//modifica los valores de las variables que mandaremos
				$this->arreglo['id_producto'] = $f['id_producto'];
				$this->arreglo['cantidad_pro'] = $f['cantidad_pro'];
				$this->arreglo['precio_afiliado'] = $f['precio_afiliado'];
				$this->arreglo['id_ficha'] = $id_ficha;
				$this->arreglo['puntos_volumen'] = $f['puntos_volumen'];
				$this->arreglo['puntos_comisionable'] = $f['puntos_comisionables'];
				$this->arreglo['precio_total'] = $f['precio_total'];
				$this->arreglo['puntos_obtenidos'] = $total_puntos;
				$this->arreglo['puntos_pagar'] = $total_pagar;


				//para agregar el usuario que compro sus puntos

				$this->arreglo['id_afiliado'] = $id_afiliado;
				$this->arreglo['id_sucursal'] = $id_sucursal;
				$this->arreglo['fecha'] = $fecha;

				$this->setParametro('id_afiliado','id_afiliado','int4');
				$this->setParametro('id_sucursal','id_sucursal','int4');
				$this->setParametro('fecha','fecha','date');

				//Define los parametros para la funcion
				$this->setParametro('id_producto', 'id_producto', 'int4');
				$this->setParametro('cantidad_pro', 'cantidad_pro', 'int4');
				$this->setParametro('precio_afiliado', 'precio_afiliado', 'numeric');
				$this->setParametro('id_ficha', 'id_ficha', 'int4');
				$this->setParametro('puntos_volumen', 'puntos_volumen', 'numeric');
				$this->setParametro('puntos_comisionable', 'puntos_comisionable', 'numeric');
				$this->setParametro('precio_total', 'precio_total', 'numeric');
				$this->setParametro('puntos_obtenidos', 'puntos_obtenidos', 'numeric');
				$this->setParametro('puntos_pagar', 'puntos_pagar', 'numeric');

				
				//Ejecuta la instruccion
				$this->armarConsulta();
				$stmt = $link->prepare($this->consulta);
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_ASSOC);

				//recupera parametros devuelto depues de insertar ... (id_solicitud)
				$resp_procedimiento = $this->divRespuesta($result['f_intermediario_ime']);

				if ($resp_procedimiento['tipo_respuesta']=='ERROR') {
					throw new Exception("Error al insertar detalle  en la bd", 3);
				}


			}// fin de foreach


		$stmt2 = $link->prepare("select bin.insertar_puntos_afiliado_por_compra('$id_ficha','$id_afiliado')");
        $stmt2->execute();




			//si todo va bien confirmamos y regresamos el resultado
			$link->commit();
			$this->respuesta=new Mensaje();
			$this->respuesta->setMensaje($resp_procedimiento['tipo_respuesta'],$this->nombre_archivo,$resp_procedimiento['mensaje'],$resp_procedimiento['mensaje_tec'],'base',$this->procedimiento,$this->transaccion,$this->tipo_procedimiento,$this->consulta);
			$this->respuesta->setDatos($respuesta);
		}
		catch (Exception $e) {
			$link->rollBack();
			$this->respuesta=new Mensaje();
			if ($e->getCode() == 3) {//es un error de un procedimiento almacenado de pxp
				$this->respuesta->setMensaje($resp_procedimiento['tipo_respuesta'],$this->nombre_archivo,$resp_procedimiento['mensaje'],$resp_procedimiento['mensaje_tec'],'base',$this->procedimiento,$this->transaccion,$this->tipo_procedimiento,$this->consulta);
			} else if ($e->getCode() == 2) {//es un error en bd de una consulta
				$this->respuesta->setMensaje('ERROR',$this->nombre_archivo,$e->getMessage(),$e->getMessage(),'modelo','','','','');
			} else {//es un error lanzado con throw exception
				throw new Exception($e->getMessage(), 2);
			}

		}

		return $this->respuesta;
	}
			
}
?>