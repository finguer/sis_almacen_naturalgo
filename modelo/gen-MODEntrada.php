<?php
/**
*@package pXP
*@file gen-MODEntrada.php
*@author  (admin)
*@date 27-03-2015 05:52:05
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODEntrada extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarEntrada(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='prod.ft_entrada_sel';
		$this->transaccion='PROD_ENT_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_entrada','int4');
		$this->captura('id_producto','int4');
		$this->captura('estado_reg','varchar');
		$this->captura('cantidad','numeric');
		$this->captura('id_usuario_ai','int4');
		$this->captura('fecha_reg','timestamp');
		$this->captura('usuario_ai','varchar');
		$this->captura('id_usuario_reg','int4');
		$this->captura('id_usuario_mod','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		
		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarEntrada(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_entrada_ime';
		$this->transaccion='PROD_ENT_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_producto','id_producto','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('cantidad','cantidad','numeric');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarEntrada(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_entrada_ime';
		$this->transaccion='PROD_ENT_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_entrada','id_entrada','int4');
		$this->setParametro('id_producto','id_producto','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('cantidad','cantidad','numeric');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarEntrada(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_entrada_ime';
		$this->transaccion='PROD_ENT_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_entrada','id_entrada','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>