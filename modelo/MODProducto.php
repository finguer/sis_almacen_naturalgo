<?php
/**
*@package pXP
*@file gen-MODProducto.php
*@author  (admin)
*@date 19-04-2015 01:11:36
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODProducto extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarProducto(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='prod.ft_producto_sel';
		$this->transaccion='PROD_produ_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_producto','int4');
		$this->captura('codigo','varchar');
		$this->captura('descripcion','varchar');
		$this->captura('id_marca','int4');
		$this->captura('puntos_comisionables','numeric');
		$this->captura('stock','int4');
		$this->captura('estado_reg','varchar');
		$this->captura('costo','numeric');
		$this->captura('precio_cliente','numeric');
		$this->captura('id_categoria','int4');
		$this->captura('puntos_volumen','numeric');
		$this->captura('nombre','varchar');
		$this->captura('precio_afiliado','numeric');
		$this->captura('id_usuario_ai','int4');
		$this->captura('id_usuario_reg','int4');
		$this->captura('usuario_ai','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		$this->captura('desc_categoria','varchar');
		$this->captura('desc_marca','varchar');


		$this->captura('precio_afiliado_original','numeric');
		$this->captura('precio_cliente_original','numeric');
		$this->captura('puntos_volumen_original','numeric');
		$this->captura('puntos_comisionables_original','numeric');
		
		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarProducto(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_producto_ime';
		$this->transaccion='PROD_produ_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('codigo','codigo','varchar');
		$this->setParametro('descripcion','descripcion','varchar');
		$this->setParametro('id_marca','id_marca','int4');
		$this->setParametro('puntos_comisionables','puntos_comisionables','numeric');
		$this->setParametro('stock','stock','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('costo','costo','numeric');
		$this->setParametro('precio_cliente','precio_cliente','numeric');
		$this->setParametro('id_categoria','id_categoria','int4');
		$this->setParametro('puntos_volumen','puntos_volumen','numeric');
		$this->setParametro('nombre','nombre','varchar');
		$this->setParametro('precio_afiliado','precio_afiliado','numeric');

		$this->setParametro('precio_afiliado_original','precio_afiliado_original','numeric');
		$this->setParametro('precio_cliente_original','precio_cliente_original','numeric');
		$this->setParametro('puntos_volumen_original','puntos_volumen_original','numeric');
		$this->setParametro('puntos_comisionables_original','puntos_comisionables_original','numeric');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarProducto(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_producto_ime';
		$this->transaccion='PROD_produ_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_producto','id_producto','int4');
		$this->setParametro('codigo','codigo','varchar');
		$this->setParametro('descripcion','descripcion','varchar');
		$this->setParametro('id_marca','id_marca','int4');
		$this->setParametro('puntos_comisionables','puntos_comisionables','numeric');
		$this->setParametro('stock','stock','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('costo','costo','numeric');
		$this->setParametro('precio_cliente','precio_cliente','numeric');
		$this->setParametro('id_categoria','id_categoria','int4');
		$this->setParametro('puntos_volumen','puntos_volumen','numeric');
		$this->setParametro('nombre','nombre','varchar');
		$this->setParametro('precio_afiliado','precio_afiliado','numeric');

		$this->setParametro('precio_afiliado_original','precio_afiliado_original','numeric');
		$this->setParametro('precio_cliente_original','precio_cliente_original','numeric');
		$this->setParametro('puntos_volumen_original','puntos_volumen_original','numeric');
		$this->setParametro('puntos_comisionables_original','puntos_comisionables_original','numeric');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarProducto(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_producto_ime';
		$this->transaccion='PROD_produ_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_producto','id_producto','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>