<?php
/**
*@package pXP
*@file gen-MODKitDetalle.php
*@author  (admin)
*@date 08-05-2015 16:08:07
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODKitDetalle extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarKitDetalle(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='prod.ft_kit_detalle_sel';
		$this->transaccion='PROD_KITDE_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_kit_detalle','int4');
		$this->captura('id_kit','int4');
		$this->captura('cantidad_producto','int4');
		$this->captura('estado_reg','varchar');
		$this->captura('id_producto','int4');
		$this->captura('id_usuario_ai','int4');
		$this->captura('id_usuario_reg','int4');
		$this->captura('fecha_reg','timestamp');
		$this->captura('usuario_ai','varchar');
		$this->captura('id_usuario_mod','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');

		$this->captura('desc_producto','varchar');


		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarKitDetalle(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_kit_detalle_ime';
		$this->transaccion='PROD_KITDE_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_kit','id_kit','int4');
		$this->setParametro('cantidad_producto','cantidad_producto','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('id_producto','id_producto','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarKitDetalle(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_kit_detalle_ime';
		$this->transaccion='PROD_KITDE_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_kit_detalle','id_kit_detalle','int4');
		$this->setParametro('id_kit','id_kit','int4');
		$this->setParametro('cantidad_producto','cantidad_producto','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('id_producto','id_producto','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarKitDetalle(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='prod.ft_kit_detalle_ime';
		$this->transaccion='PROD_KITDE_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_kit_detalle','id_kit_detalle','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>