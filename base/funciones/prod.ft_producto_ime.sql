CREATE OR REPLACE FUNCTION prod.ft_producto_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Almacen
 FUNCION: 		prod.ft_producto_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'prod.tproducto'
 AUTOR: 		 (admin)
 FECHA:	        19-04-2015 01:11:36
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_producto	integer;

BEGIN

    v_nombre_funcion = 'prod.ft_producto_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'PROD_produ_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin
 	#FECHA:		19-04-2015 01:11:36
	***********************************/

	if(p_transaccion='PROD_produ_INS')then

        begin
        	--Sentencia de la insercion
        	insert into prod.tproducto(
			codigo,
			descripcion,
			id_marca,
			puntos_comisionables,
			estado_reg,
			costo,
			precio_cliente,
			id_categoria,
			puntos_volumen,
			nombre,
			precio_afiliado,
			id_usuario_ai,
			id_usuario_reg,
			usuario_ai,
			fecha_reg,
			id_usuario_mod,
			fecha_mod,
      precio_afiliado_original,
      precio_cliente_original,
      puntos_volumen_original,
     puntos_comisionables_original
          	) values(
			v_parametros.codigo,
			v_parametros.descripcion,
			v_parametros.id_marca,
			v_parametros.puntos_comisionables,

			'activo',
			v_parametros.costo,
			v_parametros.precio_cliente,
			v_parametros.id_categoria,
			v_parametros.puntos_volumen,
			v_parametros.nombre,
			v_parametros.precio_afiliado,
			v_parametros._id_usuario_ai,
			p_id_usuario,
			v_parametros._nombre_usuario_ai,
			now(),
			null,
			null,
      v_parametros.precio_afiliado_original,
      v_parametros.precio_cliente_original,
      v_parametros.puntos_volumen_original,
      v_parametros.puntos_comisionables_original



			)RETURNING id_producto into v_id_producto;

			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Producto almacenado(a) con exito (id_producto'||v_id_producto||')');
            v_resp = pxp.f_agrega_clave(v_resp,'id_producto',v_id_producto::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_produ_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin
 	#FECHA:		19-04-2015 01:11:36
	***********************************/

	elsif(p_transaccion='PROD_produ_MOD')then

		begin
			--Sentencia de la modificacion
			update prod.tproducto set
			codigo = v_parametros.codigo,
			descripcion = v_parametros.descripcion,
			id_marca = v_parametros.id_marca,
			puntos_comisionables = v_parametros.puntos_comisionables,
			costo = v_parametros.costo,
			precio_cliente = v_parametros.precio_cliente,
			id_categoria = v_parametros.id_categoria,
			puntos_volumen = v_parametros.puntos_volumen,
			nombre = v_parametros.nombre,
			precio_afiliado = v_parametros.precio_afiliado,
			id_usuario_mod = p_id_usuario,
			fecha_mod = now(),
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai,
        precio_afiliado_original =  v_parametros.precio_afiliado_original,
        precio_cliente_original=  v_parametros.precio_cliente_original,
        puntos_volumen_original = v_parametros.puntos_volumen_original,
        puntos_comisionables_original = v_parametros.puntos_comisionables_original


			where id_producto=v_parametros.id_producto;

			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Producto modificado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_producto',v_parametros.id_producto::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_produ_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin
 	#FECHA:		19-04-2015 01:11:36
	***********************************/

	elsif(p_transaccion='PROD_produ_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from prod.tproducto
            where id_producto=v_parametros.id_producto;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Producto eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_producto',v_parametros.id_producto::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	else

    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION

	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
