CREATE OR REPLACE FUNCTION prod.ft_kit_sel(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		SISTEMA DE ALMACEN
 FUNCION: 		prod.ft_kit_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'prod.tkit'
 AUTOR: 		 (admin)
 FECHA:	        08-05-2015 16:07:12
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;

BEGIN

	v_nombre_funcion = 'prod.ft_kit_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'PROD_KITP_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin
 	#FECHA:		08-05-2015 16:07:12
	***********************************/

	if(p_transaccion='PROD_KITP_SEL')then

    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						kitp.id_kit,
						kitp.nombre,
						kitp.descripcion,
						kitp.estado_reg,
						kitp.id_usuario_ai,
						kitp.usuario_ai,
						kitp.fecha_reg,
						kitp.id_usuario_reg,
						kitp.fecha_mod,
						kitp.id_usuario_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod	,
						kitp.precio
						from prod.tkit kitp
						inner join segu.tusuario usu1 on usu1.id_usuario = kitp.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = kitp.id_usuario_mod
				        where  ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_KITP_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin
 	#FECHA:		08-05-2015 16:07:12
	***********************************/

	elsif(p_transaccion='PROD_KITP_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_kit)
					    from prod.tkit kitp
					    inner join segu.tusuario usu1 on usu1.id_usuario = kitp.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = kitp.id_usuario_mod
					    where ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;

	else

		raise exception 'Transaccion inexistente';

	end if;

EXCEPTION

	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
