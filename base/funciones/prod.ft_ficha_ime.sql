CREATE OR REPLACE FUNCTION prod.ft_ficha_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Almacen
 FUNCION: 		prod.ft_ficha_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'prod.tficha'
 AUTOR: 		 (admin)
 FECHA:	        24-04-2015 18:05:16
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_ficha	integer;
  v_numero_siguiente INTEGER;
  v_id_recibo INTEGER;

BEGIN

    v_nombre_funcion = 'prod.ft_ficha_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'PROD_FICH_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin
 	#FECHA:		24-04-2015 18:05:16
	***********************************/

	if(p_transaccion='PROD_FICH_INS')then

        begin

          select id_recibo INTO v_id_recibo  from sucu.trecibo
          where id_sucursal = v_parametros.id_sucursal ORDER BY id_recibo DESC LIMIT 1 ;

          select numero_siguiente into v_numero_siguiente
          from sucu.trecibo
          where id_recibo = v_id_recibo;
        	--Sentencia de la insercion
        	insert into prod.tficha(
			fecha,
			id_afiliado,
			estado_reg,
			codigo,
			id_usuario_ai,
			usuario_ai,
			fecha_reg,
			id_usuario_reg,
			id_usuario_mod,
			fecha_mod,
              id_sucursal
          	) values(
			v_parametros.fecha,
			v_parametros.id_afiliado,
			'activo',
			v_numero_siguiente,
			v_parametros._id_usuario_ai,
			v_parametros._nombre_usuario_ai,
			now(),
			p_id_usuario,
			null,
			null,
              v_parametros.id_sucursal



			)RETURNING id_ficha into v_id_ficha;


      UPDATE sucu.trecibo set numero_siguiente = numero_siguiente + 1
      WHERE id_recibo = v_id_recibo;

			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','ficha almacenado(a) con exito (id_ficha'||v_id_ficha||')');
            v_resp = pxp.f_agrega_clave(v_resp,'id_ficha',v_id_ficha::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_FICH_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin
 	#FECHA:		24-04-2015 18:05:16
	***********************************/

	elsif(p_transaccion='PROD_FICH_MOD')then

		begin
			--Sentencia de la modificacion
			update prod.tficha set
			fecha = v_parametros.fecha,
			id_afiliado = v_parametros.id_afiliado,
			codigo = v_parametros.codigo,
			id_usuario_mod = p_id_usuario,
			fecha_mod = now(),
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_ficha=v_parametros.id_ficha;

			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','ficha modificado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_ficha',v_parametros.id_ficha::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_FICH_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin
 	#FECHA:		24-04-2015 18:05:16
	***********************************/

	elsif(p_transaccion='PROD_FICH_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from prod.tficha
            where id_ficha=v_parametros.id_ficha;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','ficha eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_ficha',v_parametros.id_ficha::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	else

    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION

	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
