CREATE OR REPLACE FUNCTION prod.ft_producto_sel(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Almacen
 FUNCION: 		prod.ft_producto_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'prod.tproducto'
 AUTOR: 		 (admin)
 FECHA:	        19-04-2015 01:11:36
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;

BEGIN

	v_nombre_funcion = 'prod.ft_producto_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'PROD_produ_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin
 	#FECHA:		19-04-2015 01:11:36
	***********************************/

	if(p_transaccion='PROD_produ_SEL')then

    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						produ.id_producto,
						produ.codigo,
						produ.descripcion,
						produ.id_marca,
						produ.puntos_comisionables,
						produ.stock,
						produ.estado_reg,
						produ.costo,
						produ.precio_cliente,
						produ.id_categoria,
						produ.puntos_volumen,
						produ.nombre,
						produ.precio_afiliado,
						produ.id_usuario_ai,
						produ.id_usuario_reg,
						produ.usuario_ai,
						produ.fecha_reg,
						produ.id_usuario_mod,
						produ.fecha_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod,
							cat.nombre as desc_categoria,
						mar.nombre as desc_marca,
						 produ.precio_afiliado_original,
            produ.precio_cliente_original,
            produ.puntos_volumen_original,
            produ.puntos_comisionables_original
						from prod.tproducto produ
						inner join segu.tusuario usu1 on usu1.id_usuario = produ.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = produ.id_usuario_mod
						inner join prod.tcategoria cat on cat.id_categoria = produ.id_categoria
						inner join prod.tmarca mar on mar.id_marca = produ.id_marca
				        where  ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_produ_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin
 	#FECHA:		19-04-2015 01:11:36
	***********************************/

	elsif(p_transaccion='PROD_produ_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_producto)
					    from prod.tproducto produ
					    inner join segu.tusuario usu1 on usu1.id_usuario = produ.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = produ.id_usuario_mod
					    where ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;

	else

		raise exception 'Transaccion inexistente';

	end if;

EXCEPTION

	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
