CREATE OR REPLACE FUNCTION "prod"."ft_kit_detalle_ime" (	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$

/**************************************************************************
 SISTEMA:		SISTEMA DE ALMACEN
 FUNCION: 		prod.ft_kit_detalle_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'prod.tkit_detalle'
 AUTOR: 		 (admin)
 FECHA:	        08-05-2015 16:08:07
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_kit_detalle	integer;
			    
BEGIN

    v_nombre_funcion = 'prod.ft_kit_detalle_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'PROD_KITDE_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin	
 	#FECHA:		08-05-2015 16:08:07
	***********************************/

	if(p_transaccion='PROD_KITDE_INS')then
					
        begin
        	--Sentencia de la insercion
        	insert into prod.tkit_detalle(
			id_kit,
			cantidad_producto,
			estado_reg,
			id_producto,
			id_usuario_ai,
			id_usuario_reg,
			fecha_reg,
			usuario_ai,
			id_usuario_mod,
			fecha_mod
          	) values(
			v_parametros.id_kit,
			v_parametros.cantidad_producto,
			'activo',
			v_parametros.id_producto,
			v_parametros._id_usuario_ai,
			p_id_usuario,
			now(),
			v_parametros._nombre_usuario_ai,
			null,
			null
							
			
			
			)RETURNING id_kit_detalle into v_id_kit_detalle;
			
			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','kit detalle almacenado(a) con exito (id_kit_detalle'||v_id_kit_detalle||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_kit_detalle',v_id_kit_detalle::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************    
 	#TRANSACCION:  'PROD_KITDE_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin	
 	#FECHA:		08-05-2015 16:08:07
	***********************************/

	elsif(p_transaccion='PROD_KITDE_MOD')then

		begin
			--Sentencia de la modificacion
			update prod.tkit_detalle set
			id_kit = v_parametros.id_kit,
			cantidad_producto = v_parametros.cantidad_producto,
			id_producto = v_parametros.id_producto,
			id_usuario_mod = p_id_usuario,
			fecha_mod = now(),
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_kit_detalle=v_parametros.id_kit_detalle;
               
			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','kit detalle modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_kit_detalle',v_parametros.id_kit_detalle::varchar);
               
            --Devuelve la respuesta
            return v_resp;
            
		end;

	/*********************************    
 	#TRANSACCION:  'PROD_KITDE_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin	
 	#FECHA:		08-05-2015 16:08:07
	***********************************/

	elsif(p_transaccion='PROD_KITDE_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from prod.tkit_detalle
            where id_kit_detalle=v_parametros.id_kit_detalle;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','kit detalle eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_kit_detalle',v_parametros.id_kit_detalle::varchar);
              
            --Devuelve la respuesta
            return v_resp;

		end;
         
	else
     
    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION
				
	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;
				        
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "prod"."ft_kit_detalle_ime"(integer, integer, character varying, character varying) OWNER TO postgres;
