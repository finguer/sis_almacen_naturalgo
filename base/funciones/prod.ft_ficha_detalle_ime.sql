CREATE OR REPLACE FUNCTION prod.ft_ficha_detalle_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Almacen
 FUNCION: 		prod.ft_ficha_detalle_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'prod.tficha_detalle'
 AUTOR: 		 (admin)
 FECHA:	        24-04-2015 18:12:03
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

  DECLARE

    v_nro_requerimiento INTEGER;
    v_parametros        RECORD;
    v_id_requerimiento  INTEGER;
    v_resp              VARCHAR;
    v_nombre_funcion    TEXT;
    v_mensaje_error     TEXT;
    v_id_ficha_detalle  INTEGER;
    v_cantidad_producto INTEGER;

  BEGIN

    v_nombre_funcion = 'prod.ft_ficha_detalle_ime';
    v_parametros = pxp.f_get_record(p_tabla);

/*********************************
 #TRANSACCION:  'PROD_FICDE_INS'
 #DESCRIPCION:	Insercion de registros
 #AUTOR:		admin
 #FECHA:		24-04-2015 18:12:03
***********************************/

    IF (p_transaccion = 'PROD_FICDE_INS')
    THEN


      BEGIN

        SELECT stock
        INTO v_cantidad_producto
        FROM prod.tproducto
        WHERE id_producto = v_parametros.id_producto;

        if(v_parametros.cantidad_pro = 0)THEN
        RAISE EXCEPTION '%','la cantidad a vender del producto no puede ser 0';
        END IF ;

        IF (v_parametros.cantidad_pro <= v_cantidad_producto)
        THEN



--Sentencia de la insercion
          INSERT INTO prod.tficha_detalle (
            puntos_comisionable,
            estado_reg,
            puntos_pagar,
            puntos_volumen,
            cantidad,
            precio_afiliado,
            id_producto,
            puntos_obtenidos,
            id_ficha,
            usuario_ai,
            fecha_reg,
            id_usuario_reg,
            id_usuario_ai,
            id_usuario_mod,
            fecha_mod,
            precio_total
          ) VALUES (
            v_parametros.puntos_comisionable,
            'activo',
            v_parametros.puntos_pagar,
            v_parametros.puntos_volumen,
            v_parametros.cantidad_pro,
            v_parametros.precio_afiliado,
            v_parametros.id_producto,
            v_parametros.puntos_obtenidos,
            v_parametros.id_ficha,
            'SIS',
            now(),
            p_id_usuario,
            1,
            NULL,
            NULL,
            v_parametros.precio_total


          )
          RETURNING id_ficha_detalle
            INTO v_id_ficha_detalle;

          UPDATE prod.tproducto
          SET stock = stock - v_parametros.cantidad_pro
          WHERE id_producto = v_parametros.id_producto;

--Definicion de la respuesta
          v_resp = pxp.f_agrega_clave(v_resp, 'mensaje',
                                      'ficha_detalle almacenado(a) con exito (id_ficha_detalle' || v_id_ficha_detalle ||
                                      ')');
          v_resp = pxp.f_agrega_clave(v_resp, 'id_ficha_detalle', v_id_ficha_detalle :: VARCHAR);

--Devuelve la respuesta
          RETURN v_resp;

          ELSIF (v_parametros.cantidad_pro > v_cantidad_producto )THEN
            RAISE EXCEPTION '%','no exitste suficiente stock';

        END IF;



      END;

/*********************************
 #TRANSACCION:  'PROD_FICDE_MOD'
 #DESCRIPCION:	Modificacion de registros
 #AUTOR:		admin
 #FECHA:		24-04-2015 18:12:03
***********************************/

    ELSIF (p_transaccion = 'PROD_FICDE_MOD')
      THEN

        BEGIN
--Sentencia de la modificacion
          UPDATE prod.tficha_detalle
          SET
            puntos_comisionable = v_parametros.puntos_comisionable,
            puntos_pagar        = v_parametros.puntos_pagar,
            puntos_volumen      = v_parametros.puntos_volumen,
            cantidad            = v_parametros.cantidad,
            precio_afiliado     = v_parametros.precio_afiliado,
            id_producto         = v_parametros.id_producto,
            puntos_obtenidos    = v_parametros.puntos_obtenidos,
            id_ficha            = v_parametros.id_ficha,
            id_usuario_mod      = p_id_usuario,
            fecha_mod           = now(),
            id_usuario_ai       = v_parametros._id_usuario_ai,
            usuario_ai          = v_parametros._nombre_usuario_ai
          WHERE id_ficha_detalle = v_parametros.id_ficha_detalle;

--Definicion de la respuesta
          v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'ficha_detalle modificado(a)');
          v_resp = pxp.f_agrega_clave(v_resp, 'id_ficha_detalle', v_parametros.id_ficha_detalle :: VARCHAR);

--Devuelve la respuesta
          RETURN v_resp;

        END;

/*********************************
 #TRANSACCION:  'PROD_FICDE_ELI'
 #DESCRIPCION:	Eliminacion de registros
 #AUTOR:		admin
 #FECHA:		24-04-2015 18:12:03
***********************************/

    ELSIF (p_transaccion = 'PROD_FICDE_ELI')
      THEN

        BEGIN
--Sentencia de la eliminacion
          DELETE FROM prod.tficha_detalle
          WHERE id_ficha_detalle = v_parametros.id_ficha_detalle;

--Definicion de la respuesta
          v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'ficha_detalle eliminado(a)');
          v_resp = pxp.f_agrega_clave(v_resp, 'id_ficha_detalle', v_parametros.id_ficha_detalle :: VARCHAR);

--Devuelve la respuesta
          RETURN v_resp;

        END;

    ELSE

      RAISE EXCEPTION 'Transaccion inexistente: %', p_transaccion;

    END IF;

    EXCEPTION

    WHEN OTHERS THEN
      v_resp='';
      v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', SQLERRM);
      v_resp = pxp.f_agrega_clave(v_resp, 'codigo_error', SQLSTATE);
      v_resp = pxp.f_agrega_clave(v_resp, 'procedimientos', v_nombre_funcion);
      RAISE EXCEPTION '%', v_resp;

  END;
$BODY$
LANGUAGE plpgsql VOLATILE;
