CREATE OR REPLACE FUNCTION prod.ft_kit_detalle_sel(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		SISTEMA DE ALMACEN
 FUNCION: 		prod.ft_kit_detalle_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'prod.tkit_detalle'
 AUTOR: 		 (admin)
 FECHA:	        08-05-2015 16:08:07
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;

BEGIN

	v_nombre_funcion = 'prod.ft_kit_detalle_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'PROD_KITDE_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin
 	#FECHA:		08-05-2015 16:08:07
	***********************************/

	if(p_transaccion='PROD_KITDE_SEL')then

    	begin
    		--Sentencia de la consulta
			v_consulta:='select
                  kitde.id_kit_detalle,
                  kitde.id_kit,
                  kitde.cantidad_producto,
                  kitde.estado_reg,
                  kitde.id_producto,
                  kitde.id_usuario_ai,
                  kitde.id_usuario_reg,
                  kitde.fecha_reg,
                  kitde.usuario_ai,
                  kitde.id_usuario_mod,
                  kitde.fecha_mod,
                  usu1.cuenta as usr_reg,
                  usu2.cuenta as usr_mod	,
                  pro.nombre as desc_producto
                from prod.tkit_detalle kitde
                  inner join segu.tusuario usu1 on usu1.id_usuario = kitde.id_usuario_reg
                  left join segu.tusuario usu2 on usu2.id_usuario = kitde.id_usuario_mod

                  inner join prod.tproducto pro on pro.id_producto = kitde.id_producto
				        where  ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_KITDE_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin
 	#FECHA:		08-05-2015 16:08:07
	***********************************/

	elsif(p_transaccion='PROD_KITDE_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_kit_detalle)
					    from prod.tkit_detalle kitde
					    inner join segu.tusuario usu1 on usu1.id_usuario = kitde.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = kitde.id_usuario_mod
						inner join prod.tproducto pro on pro.id_producto = kitde.id_producto
					    where ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;

	else

		raise exception 'Transaccion inexistente';

	end if;

EXCEPTION

	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
