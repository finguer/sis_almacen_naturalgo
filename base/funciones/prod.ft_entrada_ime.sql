CREATE OR REPLACE FUNCTION prod.ft_entrada_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Sistema de Almacen
 FUNCION: 		prod.ft_entrada_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'prod.tentrada'
 AUTOR: 		 (admin)
 FECHA:	        27-03-2015 05:52:05
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_entrada	integer;

BEGIN

    v_nombre_funcion = 'prod.ft_entrada_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'PROD_ENT_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin
 	#FECHA:		27-03-2015 05:52:05
	***********************************/

	if(p_transaccion='PROD_ENT_INS')then

        begin
					update prod.tproducto set stock = stock + v_parametros.cantidadd
					where id_producto = v_parametros.id_producto;
        	--Sentencia de la insercion
        	insert into prod.tentrada(
			id_producto,
			estado_reg,
			cantidad,
							fecha,
			id_usuario_ai,
			fecha_reg,
			usuario_ai,
			id_usuario_reg,
			id_usuario_mod,
			fecha_mod
          	) values(
			v_parametros.id_producto,
			'activo',
			v_parametros.cantidadd,
							v_parametros.fecha,
			v_parametros._id_usuario_ai,
			now(),
			v_parametros._nombre_usuario_ai,
			p_id_usuario,
			null,
			null



			)RETURNING id_entrada into v_id_entrada;

			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Entrada almacenado(a) con exito (id_entrada'||v_id_entrada||')');
            v_resp = pxp.f_agrega_clave(v_resp,'id_entrada',v_id_entrada::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_ENT_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin
 	#FECHA:		27-03-2015 05:52:05
	***********************************/

	elsif(p_transaccion='PROD_ENT_MOD')then

		begin
			--Sentencia de la modificacion
			update prod.tentrada set
			id_producto = v_parametros.id_producto,
			cantidad = v_parametros.cantidadd,
				fecha = v_parametros.fecha,
			id_usuario_mod = p_id_usuario,
			fecha_mod = now(),
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_entrada=v_parametros.id_entrada;

			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Entrada modificado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_entrada',v_parametros.id_entrada::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_ENT_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin
 	#FECHA:		27-03-2015 05:52:05
	***********************************/

	elsif(p_transaccion='PROD_ENT_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from prod.tentrada
            where id_entrada=v_parametros.id_entrada;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Entrada eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_entrada',v_parametros.id_entrada::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	else

    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION

	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
