CREATE OR REPLACE FUNCTION prod.ft_ficha_sel(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Almacen
 FUNCION: 		prod.ft_ficha_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'prod.tficha'
 AUTOR: 		 (admin)
 FECHA:	        24-04-2015 18:05:16
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;

BEGIN

	v_nombre_funcion = 'prod.ft_ficha_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'PROD_FICH_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin
 	#FECHA:		24-04-2015 18:05:16
	***********************************/

	if(p_transaccion='PROD_FICH_SEL')then

    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						fich.id_ficha,
						fich.fecha,
						fich.id_afiliado,
						fich.estado_reg,
						fich.codigo,
						fich.id_usuario_ai,
						fich.usuario_ai,
						fich.fecha_reg,
						fich.id_usuario_reg,
						fich.id_usuario_mod,
						fich.fecha_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod,
						PERSON.nombre_completo2 as desc_person,
            PERSON.ci,
            afil.codigo as codigo_afiliado,
            suc.nombre as nombre_sucursal

						from prod.tficha fich
						inner join segu.tusuario usu1 on usu1.id_usuario = fich.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = fich.id_usuario_mod
						INNER join afi.tafiliado afil on afil.id_afiliado = fich.id_afiliado
						INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
						inner join sucu.tsucursal suc on suc.id_sucursal = fich.id_sucursal
				        where  ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_FICH_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin
 	#FECHA:		24-04-2015 18:05:16
	***********************************/

	elsif(p_transaccion='PROD_FICH_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_ficha)
					    from prod.tficha fich
					    inner join segu.tusuario usu1 on usu1.id_usuario = fich.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = fich.id_usuario_mod
					    where ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;

	else

		raise exception 'Transaccion inexistente';

	end if;

EXCEPTION

	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
