CREATE OR REPLACE FUNCTION prod.ft_kit_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		SISTEMA DE ALMACEN
 FUNCION: 		prod.ft_kit_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'prod.tkit'
 AUTOR: 		 (admin)
 FECHA:	        08-05-2015 16:07:12
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_kit	integer;

BEGIN

    v_nombre_funcion = 'prod.ft_kit_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'PROD_KITP_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin
 	#FECHA:		08-05-2015 16:07:12
	***********************************/

	if(p_transaccion='PROD_KITP_INS')then

        begin
        	--Sentencia de la insercion
        	insert into prod.tkit(
			nombre,
			descripcion,
			estado_reg,
			id_usuario_ai,
			usuario_ai,
			fecha_reg,
			id_usuario_reg,
			fecha_mod,
			id_usuario_mod,
              precio
          	) values(
			v_parametros.nombre,
			v_parametros.descripcion,
			'activo',
			v_parametros._id_usuario_ai,
			v_parametros._nombre_usuario_ai,
			now(),
			p_id_usuario,
			null,
			null,
              v_parametros.precio



			)RETURNING id_kit into v_id_kit;

			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','kit almacenado(a) con exito (id_kit'||v_id_kit||')');
            v_resp = pxp.f_agrega_clave(v_resp,'id_kit',v_id_kit::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_KITP_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin
 	#FECHA:		08-05-2015 16:07:12
	***********************************/

	elsif(p_transaccion='PROD_KITP_MOD')then

		begin
			--Sentencia de la modificacion
			update prod.tkit set
			nombre = v_parametros.nombre,
			descripcion = v_parametros.descripcion,
			fecha_mod = now(),
			id_usuario_mod = p_id_usuario,
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai,
        precio =  v_parametros.precio
			where id_kit=v_parametros.id_kit;

			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','kit modificado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_kit',v_parametros.id_kit::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'PROD_KITP_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin
 	#FECHA:		08-05-2015 16:07:12
	***********************************/

	elsif(p_transaccion='PROD_KITP_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from prod.tkit
            where id_kit=v_parametros.id_kit;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','kit eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_kit',v_parametros.id_kit::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	else

    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION

	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
