/********************************************I-DAT-FFP-PROD-0-31/12/2012********************************************/



INSERT INTO segu.tsubsistema( codigo, nombre, fecha_reg, prefijo,
  estado_reg, nombre_carpeta, id_subsis_orig)
VALUES ( 'PROD', 'SISTEMA DE ALMACEN', '2011-11-23', 'PROD', 'activo', 'SISTEMA DE ALMACEN', NULL);

----------------------------------
--COPY LINES TO data.sql FILE
---------------------------------

select pxp.f_insert_tgui ('SISTEMA DE ALMACEN', '', 'PROD', 'si', 1, '', 1, '', '', 'PROD');
select pxp.f_insert_tgui ('Marca', 'Marca', 'MAR', 'si', 1, 'sis_almacen/vista/marca/Marca.php', 2, '', 'Marca', 'PROD');
select pxp.f_insert_tgui ('Categoria', 'Categoria', 'CAT', 'si', 2, 'sis_almacen/vista/categoria/Categoria.php', 2, '', 'Categoria', 'PROD');
select pxp.f_insert_tgui ('Productos', 'Productos', 'PRODUC', 'si', 3, 'sis_almacen/vista/producto/Producto.php', 2, '', 'Producto', 'PROD');
----------------------------------
--COPY LINES TO dependencies.sql FILE
---------------------------------

select pxp.f_insert_testructura_gui ('PROD', 'SISTEMA');
select pxp.f_insert_testructura_gui ('MAR', 'PROD');
select pxp.f_insert_testructura_gui ('CAT', 'PROD');
select pxp.f_insert_testructura_gui ('PRODUC', 'PROD');

/*****************************F-DAT-FFP-PROD-0-05/08/2014*************/




/********************************************I-DAT-FFP-PROD-0-09/05/2015********************************************/

----------------------------------
--COPY LINES TO data.sql FILE
---------------------------------

--select pxp.f_insert_tgui ('ventas', 'ventas', 'VEN', 'si', 4, 'sis_almacen/vista/producto/venta.php', 2, '', 'formVenta', 'PROD');
select pxp.f_insert_tgui ('kit', 'kit', 'KI', 'si', 5, 'sis_almacen/vista/kit/Kit.php', 2, '', 'Kit', 'PROD');
----------------------------------
--COPY LINES TO dependencies.sql FILE
---------------------------------



--select pxp.f_insert_testructura_gui ('VEN', 'PROD');
select pxp.f_insert_testructura_gui ('KI', 'PROD');

/*****************************F-DAT-FFP-PROD-0-09/05/2015*************/



