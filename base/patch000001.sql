/***********************************I-SCP-FFP-PROD-1-12/04/2015****************************************/


CREATE TABLE prod.tmarca(
  id_marca SERIAL NOT NULL,

  nombre VARCHAR(250),
  CONSTRAINT tmarca_pkey PRIMARY KEY(id_marca)
) INHERITS (pxp.tbase) WITHOUT OIDS;

CREATE TABLE prod.tcategoria(
  id_categoria SERIAL NOT NULL,

  nombre VARCHAR(250),
  CONSTRAINT tcategoria_pkey PRIMARY KEY(id_categoria)
) INHERITS (pxp.tbase) WITHOUT OIDS;


CREATE TABLE prod.tproducto(
  id_producto SERIAL NOT NULL,

  nombre VARCHAR(250),
  descripcion VARCHAR(255),
  precio_compra NUMERIC(10,2) NOT NULL,
  precio_venta NUMERIC(10,2) NOT NULL,
  precio_venta_socio NUMERIC(10,2) NOT NULL,
  puntos_del_producto NUMERIC(10,2) NOT NULL,
  puntos NUMERIC(10,2) NOT NULL,
  id_marca INTEGER,
  id_categoria INTEGER,

  CONSTRAINT tproducto_pkey PRIMARY KEY(id_producto)
) INHERITS (pxp.tbase) WITHOUT OIDS;







CREATE TABLE prod.tentrada(
  id_entrada SERIAL NOT NULL,
  cantidad NUMERIC(10,2) NOT NULL,
  id_producto INTEGER,

  CONSTRAINT tentrada_pkey PRIMARY KEY(id_entrada)
) INHERITS (pxp.tbase) WITHOUT OIDS;

ALTER TABLE prod.tentrada ADD fecha DATE NOT NULL;







CREATE TABLE prod.tcotitular(
  id_cotitular SERIAL NOT NULL,

  nombre VARCHAR(250),
  apellido1 VARCHAR(255),
  apellido2 VARCHAR(255),
  domicilio NUMERIC(10,2) NOT NULL,
  relacion NUMERIC(10,2) NOT NULL,
  id_afiliado INTEGER,
  telefono INTEGER,

  CONSTRAINT tcotitular_pkey PRIMARY KEY(id_cotitular)
) INHERITS (pxp.tbase) WITHOUT OIDS;

/***********************************F-SCP-FFP-PROD-1-12/04/2015****************************************/




/***********************************I-SCP-FFP-PROD-1-18/04/2015****************************************/

DROP TABLE prod.tproducto;



CREATE TABLE prod.tproducto(
  id_producto SERIAL NOT NULL,

  nombre VARCHAR(250),
  descripcion VARCHAR(255),
  stock integer,
  codigo varchar(255),
  costo NUMERIC(10,2),
  precio_afiliado NUMERIC(10,2),
  precio_cliente NUMERIC(10,2),
  puntos_volumen INTEGER,
  puntos_comisionables INTEGER,
  id_marca INTEGER,
  id_categoria INTEGER,

  CONSTRAINT tproducto_pkey PRIMARY KEY(id_producto)
) INHERITS (pxp.tbase) WITHOUT OIDS;


ALTER TABLE prod.tproducto ALTER COLUMN stock SET DEFAULT 0;




/***********************************F-SCP-FFP-PROD-1-18/04/2015****************************************/



/***********************************I-SCP-FFP-PROD-1-24/04/2015****************************************/

CREATE TABLE prod.tficha(
  id_ficha SERIAL NOT NULL,
  codigo VARCHAR(255),
  fecha DATE,
  id_afiliado INTEGER,
  CONSTRAINT tficha_pkey PRIMARY KEY(id_ficha)
) INHERITS (pxp.tbase) WITHOUT OIDS;



CREATE TABLE prod.tficha_detalle(
  id_ficha_detalle SERIAL NOT NULL,
  id_producto VARCHAR(255),
  cantidad INTEGER,
  precio_afiliado NUMERIC(16,2),
  puntos_volumen NUMERIC(16,2),
  puntos_comisionable NUMERIC(16,2),
  puntos_obtenidos numeric(16,2),
  puntos_pagar numeric(16,2),

  id_ficha INTEGER,
  CONSTRAINT tficha_detalle_pkey PRIMARY KEY(id_ficha_detalle)
) INHERITS (pxp.tbase) WITHOUT OIDS;


ALTER TABLE prod.tficha_detalle ADD precio_total NUMERIC(16,2) NULL;




ALTER TABLE prod.tproducto ALTER COLUMN puntos_comisionables TYPE NUMERIC(16,2) USING puntos_comisionables::NUMERIC(16,2);
ALTER TABLE prod.tproducto ALTER COLUMN puntos_volumen TYPE NUMERIC(16,2) USING puntos_volumen::NUMERIC(16,2);

/***********************************F-SCP-FFP-PROD-1-24/04/2015****************************************/



/***********************************I-SCP-FFP-PROD-1-29/04/2015****************************************/

ALTER TABLE "prod"."tficha_detalle" ALTER COLUMN "id_producto" TYPE INTEGER USING "id_producto"::INTEGER;
/***********************************F-SCP-FFP-PROD-1-29/04/2015****************************************/



/***********************************I-SCP-FFP-PROD-1-03/04/2015****************************************/

ALTER TABLE prod.tficha ADD id_sucursal INT NULL;
/***********************************F-SCP-FFP-PROD-1-03/04/2015****************************************/


/***********************************I-SCP-FFP-PROD-1-08/04/2015****************************************/

CREATE TABLE prod.tkit(
  id_kit SERIAL NOT NULL,
  nombre VARCHAR(255),
  descripcion VARCHAR(255),
  CONSTRAINT tkit_pkey PRIMARY KEY(id_kit)
) INHERITS (pxp.tbase) WITHOUT OIDS;


CREATE TABLE prod.tkit_detalle(
  id_kit_detalle SERIAL NOT NULL,
  id_producto INTEGER,
  cantidad_producto INTEGER,
  id_kit INTEGER,
  CONSTRAINT tkit_detalle_pkey PRIMARY KEY(id_kit_detalle)
) INHERITS (pxp.tbase) WITHOUT OIDS;

/***********************************F-SCP-FFP-PROD-1-08/04/2015****************************************/





/***********************************I-SCP-FFP-PROD-1-09/05/2015****************************************/

ALTER TABLE prod.tkit ADD precio NUMERIC NULL;

/***********************************F-SCP-FFP-PROD-1-09/05/2015****************************************/

/***********************************I-SCP-FFP-PROD-1-14/05/2015****************************************/

ALTER TABLE prod.tproducto ADD precio_afiliado_original  NUMERIC(10,2) NULL;
ALTER TABLE prod.tproducto ADD precio_cliente_original  NUMERIC(10,2) NULL;
ALTER TABLE prod.tproducto ADD puntos_volumen_original  NUMERIC(10,2) NULL;
ALTER TABLE prod.tproducto ADD puntos_comisionables_original  NUMERIC(10,2) NULL;

/***********************************F-SCP-FFP-PROD-1-14/05/2015****************************************/