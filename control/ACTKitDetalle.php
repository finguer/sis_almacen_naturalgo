<?php
/**
*@package pXP
*@file gen-ACTKitDetalle.php
*@author  (admin)
*@date 08-05-2015 16:08:07
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTKitDetalle extends ACTbase{    
			
	function listarKitDetalle(){
		$this->objParam->defecto('ordenacion','id_kit_detalle');

		$this->objParam->defecto('dir_ordenacion','asc');

		if($this->objParam->getParametro('id_kit')!=''){
			$this->objParam->addFiltro("kitde.id_kit = ''".$this->objParam->getParametro('id_kit')."''");
		}


		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODKitDetalle','listarKitDetalle');
		} else{
			$this->objFunc=$this->create('MODKitDetalle');
			
			$this->res=$this->objFunc->listarKitDetalle($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarKitDetalle(){
		$this->objFunc=$this->create('MODKitDetalle');	
		if($this->objParam->insertar('id_kit_detalle')){
			$this->res=$this->objFunc->insertarKitDetalle($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarKitDetalle($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarKitDetalle(){
			$this->objFunc=$this->create('MODKitDetalle');	
		$this->res=$this->objFunc->eliminarKitDetalle($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>