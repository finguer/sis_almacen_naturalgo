<?php
/**
*@package pXP
*@file gen-ACTKit.php
*@author  (admin)
*@date 08-05-2015 16:07:12
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTKit extends ACTbase{    
			
	function listarKit(){
		$this->objParam->defecto('ordenacion','id_kit');

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODKit','listarKit');
		} else{
			$this->objFunc=$this->create('MODKit');
			
			$this->res=$this->objFunc->listarKit($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarKit(){
		$this->objFunc=$this->create('MODKit');	
		if($this->objParam->insertar('id_kit')){
			$this->res=$this->objFunc->insertarKit($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarKit($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarKit(){
			$this->objFunc=$this->create('MODKit');	
		$this->res=$this->objFunc->eliminarKit($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>