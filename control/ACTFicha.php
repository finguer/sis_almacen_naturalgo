<?php
/**
*@package pXP
*@file gen-ACTFicha.php
*@author  (admin)
*@date 24-04-2015 18:05:16
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTFicha extends ACTbase{    
			
	function listarFicha(){
		$this->objParam->defecto('ordenacion','id_ficha');

		$this->objParam->defecto('dir_ordenacion','asc');


		if($this->objParam->getParametro('id_ficha')!=''){
			$this->objParam->addFiltro("fich.id_ficha = ''".$this->objParam->getParametro('id_ficha')."''");
		}

		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODFicha','listarFicha');
		} else{
			$this->objFunc=$this->create('MODFicha');
			
			$this->res=$this->objFunc->listarFicha($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarFicha(){
		$this->objFunc=$this->create('MODFicha');	
		if($this->objParam->insertar('id_ficha')){
			$this->res=$this->objFunc->insertarFicha($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarFicha($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarFicha(){
			$this->objFunc=$this->create('MODFicha');	
		$this->res=$this->objFunc->eliminarFicha($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}



	function insertarFichaCompleta(){
		$this->objFunc=$this->create('MODFicha');

		$this->res=$this->objFunc->insertarFichaCompleta($this->objParam);

		$this->res->imprimirRespuesta($this->res->generarJson());
	}




	function generarFicha(){

		$this->objParam->defecto('ordenacion','id_ficha');

		$this->objParam->defecto('dir_ordenacion','asc');
		$this->objParam->addFiltro("fich.id_ficha = ''".$this->objParam->getParametro('id_ficha')."''");
		$this->objFunc=$this->create('MODFicha');

		$this->res=$this->objFunc->listarFicha($this->objParam);


		//var_dump($this->res);
		$ficha = $this->res->getDatos();
	;


		$this->objParam->parametros_consulta['filtro'] = ' 0 = 0 ';



		//listamos detalle de la ficha

		$this->objParam->addFiltro("ficde.id_ficha = ''".$this->objParam->getParametro('id_ficha')."''");
		$this->objFunc2=$this->create('MODFichaDetalle');
		$this->res2=$this->objFunc2->listarFichaDetalle($this->objParam);


		$detalles = $this->res2->getDatos();


		$html = "";
		$i = 0;

		//$V = new EnLetras();






			setlocale(LC_ALL,"es_ES@euro","es_ES","esp");



		$html = '<!doctype html>
					<html>
					<head>
						<link media="print" rel="stylesheet" type="text/css">

						<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
						<title>Liquidacion</title>
						<style type="text/css" media="print">
							@page {
								margin: 0;
							}
						</style>
						<style>
							@page {
								215, 9mm 279, 4mm ;
							}

							@media print {
								@page {
									215, 9mm 279, 4mm ;
								}

								body {
									font-size: 12pt;
									margin-left: 5mm;
									margin-right: 5mm;
									/* font: fit-to-print;*/
								}

								body {
									font-size: 12pt;

									font-family: "Tahoma", Consolas, "Andale Mono", "Lucida Console", "Lucida Sans Typewriter", Monaco, "Courier New", monospace;
								}

								.line {
									border-bottom: 1px dotted black;
									position: relative;
								}

								.line span {
									display: inline-block;
									position: relative;
									background: white;
									bottom: -2px;
									height: 100%;
									padding: 0 5px;
								}

								.line .price {
									position: absolute;
									right: 0;
								}

								.clearfix {
									width: 100%;
								}

								.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
									float: left;
								}

								.col-sm-12 {
									width: 100%;
								}

								.col-sm-11 {
									width: 91.66666666666666%;
								}

								.col-sm-10 {
									width: 83.33333333333334%;
								}

								.col-sm-9 {
									width: 75%;
								}

								.col-sm-8 {
									width: 66.66666666666666%;
								}

								.col-sm-7 {
									width: 58.333333333333336%;
								}

								.col-sm-6 {
									width: 50%;
								}

								.col-sm-5 {
									width: 41.66666666666667%;
								}

								.col-sm-4 {
									width: 33.33333333333333%;
								}

								.col-sm-3 {
									width: 25%;
								}

								.col-sm-2 {
									width: 16.666666666666664%;
								}

								.col-sm-1 {
									width: 8.333333333333332%;
								}

								.center {
									text-align: center;
								}

								.derecha {
									text-align: right;
								}

								table {
									line-height: 1;
									border-collapse: separate;
									position: relative;
									overflow: auto;
								}


							}

							.center {
								text-align: center;
							}
						</style>
					</head>
					<body onload="window.print();">


					<div class="clearfix">


						<div class="clearfix">
							<p>
							</p>
							<table border="0" class="col-sm-12">
								<tbody>
								<tr style="color: #ffffff; display: none;">
									<td>1</td>
									<td>2</td>
									<td>3</td>
									<td>4</td>
									<td>5</td>
									<td>6</td>
									<td>7</td>
									<td>8</td>
									<td>9</td>
									<td>10</td>
									<td>11</td>
									<td>12</td>
								</tr>
								<tr>
									<td colspan="8">NaturalGO</td>
									<td colspan="4" class="derecha">Fecha: 24/01/2015</td>
								</tr>
								<tr>
									<td colspan="12">NOMBRE: '.$ficha[0]['desc_person'].'</td>
								</tr>

								<tr>
									<td colspan="8">CI : '.$ficha[0]['ci'].'</td>
									<td colspan="4" class="derecha">&nbsp;</td>
								</tr>

								<tr>
									<td colspan="8"> CODIGO AFILIADO : '.$ficha[0]['codigo_afiliado'].'</td>
									<td colspan="4" class="derecha">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="8">Orden/Ficha : '.$ficha[0]['codigo'].'</td>
									<td colspan="4" class="derecha">&nbsp;</td>
								</tr>

								<tr>
									<td colspan="8">Bodega: '.$ficha[0]['nombre_sucursal'].'</td>
									<td colspan="4" class="derecha">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="8">INFORMACION DE DISTRIBUIDOR</td>
									<td align="center" colspan="4" class="derecha">INFORMACION DE ENVIO</td>
								</tr>
								<tr>
									<td colspan="8">&nbsp;</td>
									<td colspan="4" class="derecha">&nbsp;</td>
								</tr>


								<tr>
									<td colspan="12">
										<div class="line"></div>
									</td>
								</tr>


								<tr>
									<td colspan="2">CODIGO</td>
									<td colspan="2">CANTIDAD</td>
									<td colspan="2">DESCRIPCION</td>
									<td colspan="2">PUNTOS</td>
									<td colspan="2">P.UNITARIO</td>
									<td colspan="2">PRECIO</td>
								</tr>


								<tr>
									<td colspan="12">
										<div class="line"></div>
									</td>
								</tr>';
								$puntos_total = 0;
								$total_dinero = 0;
							foreach ($detalles as $item_detalle) {
								$html .= '<tr>
								<td colspan="2">'.$item_detalle['codigo'].'</td>
								<td colspan="2">'.$item_detalle['cantidad'].'</td>
								<td colspan="2">'.$item_detalle['descripcion'].'</td>
								<td colspan="2">'.$item_detalle['puntos_obtenidos'].'</td>
								<td colspan="2">'.$item_detalle['precio_afiliado'].'</td>
								<td colspan="2">'.$item_detalle['precio_total'].'</td>

							  </tr>';

								$puntos_total = $puntos_total + $item_detalle['puntos_obtenidos'];
								$total_dinero = $total_dinero + $item_detalle['precio_total'];
								$impuestos = $total_dinero * 0.15;
								$precio_sin_impuesto = $total_dinero - $impuestos;

							}
							$html.='<tr>
									<td colspan="10" align="right"><span style="text-align: right;">TOTAL PUNTOS</span></td>
									<td colspan="2">'.number_format($puntos_total, 2, '.', '').'</td>
								</tr>
								<tr>
									<td colspan="10" align="right">IMPUESTOS</td>
									<td colspan="2">'.number_format($impuestos, 2, '.', '').'</td>
								</tr>
								<tr>
									<td colspan="10" align="right">PRECIO</td>
									<td colspan="2">'.number_format($precio_sin_impuesto, 2, '.', '').'</td>
								</tr>
								<tr>
									<td colspan="10" align="right">TOTAL</td>
									<td colspan="2">'.number_format($total_dinero, 2, '.', '').'</td>
								</tr>
								<tr>
									<td colspan="6">
										SON:SETENTA Y DOS 00/100
										BOLIVIANOS
									</td>
									<td colspan="6">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td colspan="2">&nbsp;</td>
									<td colspan="2">&nbsp;</td>
									<td colspan="2">&nbsp;</td>
									<td colspan="2">&nbsp;</td>
									<td colspan="2">&nbsp;</td>
								</tr>


								<tr>
									<td colspan="12">
										<div class="line"></div>
									</td>
								</tr>


								<tr>
									<td class="center" colspan="12">Encargado: '.$ficha[0]['usr_reg'].'</td>
								</tr>
								<tr>
									<td class="center" colspan="12"><span style="text-align: center;width:350px;">GRACIAS POR SU PREFERENCIA ! </span>
									</td>
								</tr>
								<tr>
									<td colspan="12">
										<div class="line"></div>
									</td>
								</tr>


								</tbody>
							</table>


							<p></p>


						</div>


					</div>
					</body>
					</html>';





			$temp[] = $html;
			$i++;



		$this->res->setDatos($temp);
		$this->res->imprimirRespuesta($this->res->generarJson());

	}


}

?>