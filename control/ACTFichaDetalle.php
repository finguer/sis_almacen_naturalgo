<?php
/**
*@package pXP
*@file gen-ACTFichaDetalle.php
*@author  (admin)
*@date 24-04-2015 18:12:03
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTFichaDetalle extends ACTbase{    
			
	function listarFichaDetalle(){
		$this->objParam->defecto('ordenacion','id_ficha_detalle');

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODFichaDetalle','listarFichaDetalle');
		} else{
			$this->objFunc=$this->create('MODFichaDetalle');
			
			$this->res=$this->objFunc->listarFichaDetalle($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarFichaDetalle(){
		$this->objFunc=$this->create('MODFichaDetalle');	
		if($this->objParam->insertar('id_ficha_detalle')){
			$this->res=$this->objFunc->insertarFichaDetalle($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarFichaDetalle($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}

						
	function eliminarFichaDetalle(){
			$this->objFunc=$this->create('MODFichaDetalle');	
		$this->res=$this->objFunc->eliminarFichaDetalle($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>