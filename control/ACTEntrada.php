<?php
/**
*@package pXP
*@file gen-ACTEntrada.php
*@author  (admin)
*@date 27-03-2015 05:52:05
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTEntrada extends ACTbase{    
			
	function listarEntrada(){
		$this->objParam->defecto('ordenacion','id_entrada');


		if($this->objParam->getParametro('id_producto')!=''){
			$this->objParam->addFiltro("ent.id_producto = ''".$this->objParam->getParametro('id_producto')."''");
		}

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODEntrada','listarEntrada');
		} else{
			$this->objFunc=$this->create('MODEntrada');
			
			$this->res=$this->objFunc->listarEntrada($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarEntrada(){
		$this->objFunc=$this->create('MODEntrada');	
		if($this->objParam->insertar('id_entrada')){
			$this->res=$this->objFunc->insertarEntrada($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarEntrada($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarEntrada(){
			$this->objFunc=$this->create('MODEntrada');	
		$this->res=$this->objFunc->eliminarEntrada($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>