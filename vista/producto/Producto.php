<?php
/**
*@package pXP
*@file gen-Producto.php
*@author  (admin)
*@date 19-04-2015 01:11:36
*@description Archivo con la interfaz de usuario que permite la ejecucion de todas las funcionalidades del sistema
*/

header("content-type: text/javascript; charset=UTF-8");
?>
<script>
Phx.vista.Producto=Ext.extend(Phx.gridInterfaz,{

	constructor:function(config){
		this.maestro=config.maestro;
    	//llama al constructor de la clase padre
		Phx.vista.Producto.superclass.constructor.call(this,config);

		this.init();
		this.iniciarEventos();
		this.load({params:{start:0, limit:this.tam_pag}})
	},

		iniciarEventos: function () {

			console.log(this);


			this.Cmp.costo.on('keyup', function () {

				var total = (parseInt(263)* parseFloat(this.Cmp.costo.getValue() / 100));
				this.Cmp.precio_afiliado.setValue(total);
				this.Cmp.precio_afiliado_original.setValue(total);

				var total = (parseInt(394)* parseFloat(this.Cmp.costo.getValue() / 100));
				this.Cmp.precio_cliente.setValue(total);
				this.Cmp.precio_cliente_original.setValue(total);

				var pVol = (this.Cmp.precio_afiliado.getValue() * 800 ) / 7700;

				this.Cmp.puntos_volumen.setValue(pVol);
				this.Cmp.puntos_volumen_original.setValue(pVol);

				var pCo = (this.Cmp.precio_afiliado.getValue() * 60 ) / 700;

				this.Cmp.puntos_comisionables.setValue(pCo);
				this.Cmp.puntos_comisionables_original.setValue(pCo);


			 }, this);



			/*this.Cmp.precio_afiliado.on('keyup', function () {

				var pVol = (this.Cmp.precio_afiliado.getValue() * 800 ) / 7700;

				this.Cmp.puntos_volumen.setValue(pVol);

				var pCo = (this.Cmp.precio_afiliado.getValue() * 60 ) / 700;

				this.Cmp.puntos_comisionables.setValue(pCo);




			}, this);*/






		},
			
	Atributos:[
		{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_producto'
			},
			type:'Field',
			form:true 
		},
		{
			config:{
				name: 'codigo',
				fieldLabel: 'codigo',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'produ.codigo',type:'string'},
				id_grupo:1,
				grid:true,
				form:true
		},

		{
			config:{
				name: 'nombre',
				fieldLabel: 'nombre',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:250
			},
			type:'TextField',
			filters:{pfiltro:'produ.nombre',type:'string'},
			id_grupo:1,
			grid:true,
			form:true
		},

		{
			config:{
				name: 'descripcion',
				fieldLabel: 'descripcion',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'produ.descripcion',type:'string'},
				id_grupo:1,
				grid:true,
				form:true
		},

		{
			config:{
				name: 'stock',
				fieldLabel: 'stock',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
			type:'NumberField',
			filters:{pfiltro:'produ.stock',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:false
		},

		{
			config: {
				name: 'id_marca',
				fieldLabel: 'Marca',
				allowBlank: true,
				emptyText: 'Elija una opción...',
				store: new Ext.data.JsonStore({
					url: '../../sis_almacen/control/Marca/listarMarca',
					id: 'id_marca',
					root: 'datos',
					sortInfo: {
						field: 'nombre',
						direction: 'ASC'
					},
					totalProperty: 'total',
					fields: ['id_marca', 'nombre'],
					remoteSort: true,
					baseParams: {par_filtro: 'movtip.nombre#movtip.codigo'}
				}),
				valueField: 'id_marca',
				displayField: 'nombre',
				gdisplayField: 'desc_marca',
				hiddenName: 'id_marca',
				forceSelection: true,
				typeAhead: false,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'remote',
				pageSize: 15,
				queryDelay: 1000,
				anchor: '100%',
				gwidth: 150,
				minChars: 2,
				renderer : function(value, p, record) {
					return String.format('{0}', record.data['desc_marca']);
				}
			},
			type: 'ComboBox',
			id_grupo: 0,
			filters: {pfiltro: 'movtip.nombre',type: 'string'},
			grid: true,
			form: true
		},

		{
			config: {
				name: 'id_categoria',
				fieldLabel: 'id_categoria',
				allowBlank: true,
				emptyText: 'Elija una opción...',
				store: new Ext.data.JsonStore({
					url: '../../sis_almacen/control/Categoria/listarCategoria',
					id: 'id_categoria',
					root: 'datos',
					sortInfo: {
						field: 'nombre',
						direction: 'ASC'
					},
					totalProperty: 'total',
					fields: ['id_categoria', 'nombre'],
					remoteSort: true,
					baseParams: {par_filtro: 'movtip.nombre#movtip.codigo'}
				}),
				valueField: 'id_categoria',
				displayField: 'nombre',
				gdisplayField: 'desc_categoria',
				hiddenName: 'id_categoria',
				forceSelection: true,
				typeAhead: false,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'remote',
				pageSize: 15,
				queryDelay: 1000,
				anchor: '100%',
				gwidth: 150,
				minChars: 2,
				renderer : function(value, p, record) {
					return String.format('{0}', record.data['desc_categoria']);
				}
			},
			type: 'ComboBox',
			id_grupo: 0,
			filters: {pfiltro: 'movtip.nombre',type: 'string'},
			grid: true,
			form: true
		},

		{
			config:{
				name: 'costo',
				fieldLabel: 'costo',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:655362,
				enableKeyEvents: true
			},
			type:'NumberField',
			filters:{pfiltro:'produ.costo',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},

		{
			config:{
				name: 'precio_afiliado_original',
				fieldLabel: 'precio_afiliado_original',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:655362,
				enableKeyEvents: true,
				disabled:true
			},
			type:'NumberField',
			filters:{pfiltro:'produ.precio_afiliado_original',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},

		{
			config:{
				name: 'precio_afiliado',
				fieldLabel: 'precio_afiliado',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:655362,
				enableKeyEvents: true
			},
			type:'NumberField',
			filters:{pfiltro:'produ.precio_afiliado',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},

		{
			config:{
				name: 'precio_cliente_original',
				fieldLabel: 'precio_cliente_original',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:655362,
				disabled:true
			},
			type:'NumberField',
			filters:{pfiltro:'produ.precio_cliente_original',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},


		{
			config:{
				name: 'precio_cliente',
				fieldLabel: 'precio_cliente',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:655362
			},
			type:'NumberField',
			filters:{pfiltro:'produ.precio_cliente',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},


		{
			config:{
				name: 'puntos_volumen_original',
				fieldLabel: 'PVO',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:655362,
				disabled:true
			},
			type:'NumberField',
			filters:{pfiltro:'produ.puntos_volumen_original',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},


		{
			config:{
				name: 'puntos_volumen',
				fieldLabel: 'PV',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
			type:'NumberField',
			filters:{pfiltro:'produ.puntos_volumen',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},


		{
			config:{
				name: 'puntos_comisionables_original',
				fieldLabel: 'CVO',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4,
				disabled:true
			},
			type:'NumberField',
			filters:{pfiltro:'produ.puntos_comisionables_original',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},



		{
			config:{
				name: 'puntos_comisionables',
				fieldLabel: 'CV',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'NumberField',
				filters:{pfiltro:'produ.puntos_comisionables',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},

		{
			config:{
				name: 'estado_reg',
				fieldLabel: 'Estado Reg.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:10
			},
				type:'TextField',
				filters:{pfiltro:'produ.estado_reg',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},






		{
			config:{
				name: 'id_usuario_ai',
				fieldLabel: '',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'produ.id_usuario_ai',type:'numeric'},
				id_grupo:1,
				grid:false,
				form:false
		},
		{
			config:{
				name: 'usr_reg',
				fieldLabel: 'Creado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu1.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usuario_ai',
				fieldLabel: 'Funcionaro AI',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:300
			},
				type:'TextField',
				filters:{pfiltro:'produ.usuario_ai',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'fecha_reg',
				fieldLabel: 'Fecha creación',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'produ.fecha_reg',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usr_mod',
				fieldLabel: 'Modificado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu2.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'fecha_mod',
				fieldLabel: 'Fecha Modif.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'produ.fecha_mod',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		}
	],
	tam_pag:50,	
	title:'Producto',
	ActSave:'../../sis_almacen/control/Producto/insertarProducto',
	ActDel:'../../sis_almacen/control/Producto/eliminarProducto',
	ActList:'../../sis_almacen/control/Producto/listarProducto',
	id_store:'id_producto',
	fields: [
		{name:'id_producto', type: 'numeric'},
		{name:'codigo', type: 'string'},
		{name:'descripcion', type: 'string'},
		{name:'id_marca', type: 'numeric'},
		{name:'puntos_comisionables', type: 'numeric'},
		{name:'stock', type: 'numeric'},
		{name:'estado_reg', type: 'string'},
		{name:'costo', type: 'numeric'},
		{name:'precio_cliente', type: 'numeric'},
		{name:'id_categoria', type: 'numeric'},
		{name:'puntos_volumen', type: 'numeric'},
		{name:'nombre', type: 'string'},
		{name:'precio_afiliado', type: 'numeric'},
		{name:'id_usuario_ai', type: 'numeric'},
		{name:'id_usuario_reg', type: 'numeric'},
		{name:'usuario_ai', type: 'string'},
		{name:'fecha_reg', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'id_usuario_mod', type: 'numeric'},
		{name:'fecha_mod', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'usr_reg', type: 'string'},
		{name:'usr_mod', type: 'string'},

		{name:'desc_categoria', type: 'string'},
		{name:'desc_marca', type: 'string'},



		{name:'precio_afiliado_original', type: 'numeric'},
		{name:'precio_cliente_original', type: 'numeric'},
		{name:'puntos_volumen_original', type: 'numeric'},
		{name:'puntos_comisionables_original', type: 'numeric'},


		
	],
	sortInfo:{
		field: 'id_producto',
		direction: 'ASC'
	},
	bdel:true,
	bsave:true,

	east:{
		url:'../../../sis_almacen/vista/entrada/Entrada.php',
		title:'Columnas',
		width:300,
		cls:'Entrada'
	}


	}
)
</script>
		
		