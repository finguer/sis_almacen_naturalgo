<?php
/**
*@package pXP
*@file    venta.php
*@author  Rensi Arteaga Copari 
*@date    30-01-2014
*@description permites subir archivos a la tabla de documento_sol
*/
header("content-type: text/javascript; charset=UTF-8");
?>

<script>
Phx.vista.formVenta=Ext.extend(Phx.frmInterfaz,{
    ActSave:'../../sis_almacen/control/Ficha/insertarFichaCompleta',
    tam_pag: 10,
    //layoutType: 'wizard',
    layout: 'fit',
    autoScroll: false,
    breset: false,
    labelSubmit: '<i class="fa fa-check"></i> Siguiente',
    constructor:function(config)
    {   
    	
    	//declaracion de eventos
        this.addEvents('beforesave');
        this.addEvents('successsave');
    	
    	this.buildComponentesDetalle();
        this.buildDetailGrid();
        this.buildGrupos();
        
        Phx.vista.formVenta.superclass.constructor.call(this,config);
        this.init();    
        this.iniciarEventos();
        this.iniciarEventosDetalle();
        this.onNew();
        
       // this.Cmp.tipo_concepto.store.loadData(this.arrayStore['Bien'].concat(this.arrayStore['Servicio']));
        
    },
    buildComponentesDetalle: function(){
    	
    	this.detCmp = {
    		       'id_producto': new Ext.form.ComboBox({
							                name: 'id_producto',
							                msgTarget: 'title',
							                fieldLabel: 'Producto',
							                allowBlank: false,
							                emptyText : 'Concepto...',
							                store : new Ext.data.JsonStore({
							                            url:'../../sis_almacen/control/Producto/listarProducto',
							                            id : 'id_producto',
							                            root: 'datos',
							                            sortInfo:{
							                                    field: 'id_producto',
							                                    direction: 'ASC'
							                            },
							                            totalProperty: 'total',
							                            fields: ['id_producto','codigo','descripcion','nombre','costo','precio_afiliado','precio_cliente','puntos_comisionables','puntos_volumen'],
							                            remoteSort: true,
							                            //baseParams:{par_filtro:'codigo#produ.codigo'}
							                }),
							               valueField: 'id_producto',
							               displayField: 'codigo',
							               hiddenName: 'id_producto',
							               forceSelection:true,
							               typeAhead: false,
							               triggerAction: 'all',
							               listWidth:500,
							               resizable:true,
							               lazyRender:true,
							               mode:'remote',
							               pageSize:10,
							               queryDelay:1000,
							               minChars:2,
							               qtip:'ventas de producto',
							               tpl: '<tpl for="."><div class="x-combo-list-item"><p><b>{codigo}</b></p><strong>{nombre}</strong><p>Des: {descripcion}</p></div></tpl>',
							             }),
							             
							             
	              'puntos_volumen': new Ext.form.NumberField({
										name: 'puntos_volumen',
										msgTarget: 'title',
						                fieldLabel: 'puntos_volumen',
						                allowBlank: false,
						                allowDecimals: false,
						                maxLength:10
								}),
								
				'puntos_comisionables': new Ext.form.NumberField({
										name: 'puntos_comisionables',
										msgTarget: 'title',
						                fieldLabel: 'puntos_comisionables',
						                allowBlank: false,
						                allowDecimals: false,
						                maxLength:10
								}),
								
	             'precio_afiliado': new Ext.form.NumberField({
										name: 'precio_afiliado',
										msgTarget: 'title',
						                fieldLabel: 'Cantidad',
						                allowBlank: false,
						                allowDecimals: false,
						                maxLength:10
								}),


			'total_puntos': new Ext.form.NumberField({
				name: 'total_puntos',
				msgTarget: 'title',
				fieldLabel: 'Total Puntos',
				allowBlank: false,
				allowDecimals: false,
				maxLength:10
			}),
						            
				
						            
					'cantidad_pro': new Ext.form.NumberField({
										name: 'cantidad',
										msgTarget: 'title',
						                fieldLabel: 'Cantidad',
						                allowBlank: false,
						                allowDecimals: false,
						                maxLength:10
								}),
									
					'precio_total': new Ext.form.NumberField({
									    name: 'precio_total',
									    msgTarget: 'title',
									    readOnly: true,
									    allowBlank: true
                      		 	})
					
			  }
    		
    		
    }, 
    iniciarEventosDetalle: function(){
    	
        
        this.detCmp.precio_afiliado.on('valid',function(field){
             var pTot = this.detCmp.cantidad_pro.getValue() *this.detCmp.precio_afiliado.getValue();

			var pToPuntos = this.detCmp.cantidad_pro.getValue() * this.detCmp.puntos_volumen.getValue();
             this.detCmp.precio_total.setValue(pTot);
			this.detCmp.total_puntos.setValue(pToPuntos);
            } ,this);
        
       this.detCmp.cantidad_pro.on('valid',function(field){
            var pTot = this.detCmp.cantidad_pro.getValue() * this.detCmp.precio_afiliado.getValue();

		   var pToPuntos = this.detCmp.cantidad_pro.getValue() * this.detCmp.puntos_volumen.getValue();

            this.detCmp.precio_total.setValue(pTot);
            this.detCmp.total_puntos.setValue(pToPuntos);

        } ,this);
        
        this.detCmp.id_producto.on('select',function( cmb, rec, ind){
        	alert('llega');
        	console.log(rec.data.precio_afiliado);
        	this.detCmp.precio_afiliado.setValue(rec.data.precio_afiliado);
        	this.detCmp.puntos_volumen.setValue(rec.data.puntos_volumen);
        	this.detCmp.puntos_comisionables.setValue(rec.data.puntos_comisionables);
        },this);
        
	        
	    /*this.detCmp.id_concepto_ingas.on('select',function( cmb, rec, ind){
	        	
	        	    this.detCmp.id_orden_trabajo.store.baseParams = {
			        		                                           filtro_ot:rec.data.filtro_ot,
			        		 										   requiere_ot:rec.data.requiere_ot,
			        		 										   id_grupo_ots:rec.data.id_grupo_ots
			        		 										 };
			        this.detCmp.id_orden_trabajo.modificado = true;
			        if(rec.data.requiere_ot =='obligatorio'){
			        	this.detCmp.id_orden_trabajo.allowBlank = false;
			        	this.detCmp.id_orden_trabajo.setReadOnly(false);
			        }
			        else{
			        	this.detCmp.id_orden_trabajo.allowBlank = true;
			        	this.detCmp.id_orden_trabajo.setReadOnly(true);
			        }
			        this.detCmp.id_orden_trabajo.reset();
			        
        	
             },this);*/
    },
    
    onInitAdd: function(){
    	
    	
    },
    onCancelAdd: function(re,save){
    	if(this.sw_init_add){
    		this.mestore.remove(this.mestore.getAt(0));
    	}
    	
    	this.sw_init_add = false;
    	this.evaluaGrilla();
    },
    onUpdateRegister: function(){
    	this.sw_init_add = false;
    },
    
    onAfterEdit:function(re, o, rec, num){
    	//set descriptins values ...  in combos boxs
    	
    	var cmb_rec = this.detCmp['id_producto'].store.getById(rec.get('id_producto'));
    	if(cmb_rec){
    		rec.set('codigo', cmb_rec.get('codigo')); 
    	}
    	
    },
    
    evaluaRequistos: function(){
    	//valida que todos los requistosprevios esten completos y habilita la adicion en el grid
     	var i = 0;
    	sw = true
    	while( i < this.Componentes.length) {
    		
    		if(!this.Componentes[i].isValid()){
    		   sw = false;
    		   //i = this.Componentes.length;
    		}
    		i++;
    	}
    	
   
    	
    	return sw
    },
    
    bloqueaRequisitos: function(sw){
    	//this.Cmp.id_depto.setDisabled(sw);
    	//this.Cmp.id_moneda.setDisabled(sw);
    	
    	//this.Cmp.tipo_concepto.setDisabled(sw);
    	//this.Cmp.fecha_soli.setDisabled(sw);
    	this.cargarDatosMaestro();
    	
    	
    },
    
    cargarDatosMaestro: function(){
    	
        
       /* this.detCmp.id_orden_trabajo.store.baseParams.fecha_solicitud = this.Cmp.fecha_soli.getValue().dateFormat('d/m/Y');
        this.detCmp.id_orden_trabajo.modificado = true;
        
        this.detCmp.id_centro_costo.store.baseParams.id_gestion = this.Cmp.id_gestion.getValue();
        this.detCmp.id_centro_costo.store.baseParams.codigo_subsistema = 'ADQ';
        this.detCmp.id_centro_costo.store.baseParams.id_depto = this.Cmp.id_depto.getValue();
        this.detCmp.id_centro_costo.modificado = true;
        //cuando esta el la inteface de presupeustos no filtra por bienes o servicios
        this.detCmp.id_concepto_ingas.store.baseParams.tipo=this.Cmp.tipo.getValue();
        this.detCmp.id_concepto_ingas.store.baseParams.id_gestion=this.Cmp.id_gestion.getValue();
        this.detCmp.id_concepto_ingas.modificado = true;*/
    	
    },
    
    evaluaGrilla: function(){
    	//al eliminar si no quedan registros en la grilla desbloquea los requisitos en el maestro
    	var  count = this.mestore.getCount();
    	if(count == 0){
    		this.bloqueaRequisitos(false);
    	} 
    },
    
    
    buildDetailGrid: function(){
    	
    	//cantidad,detalle,peso,totalo
        var Items = Ext.data.Record.create([{
                        name: 'cantidad_pro',
                        type: 'int'
                    }, {
                        name: 'id_producto',
                        type: 'int'
                    }, {
                        name: 'puntos_volumen',
                        type: 'int'
                    }, {
                        name: 'punto_comisionables',
                        type: 'int'
                    },{
                        name: 'precio_afiliado',
                        type: 'float'
                    },{
                        name: 'precio_total',
                        type: 'float'
                    },{
						name: 'total_puntos',
						type: 'float'
					}
                    ]);
        
        this.mestore = new Ext.data.JsonStore({
					url: '../../sis_adquisiciones/control/SolicitudDet/listarSolicitudDet',
					id: 'id_solicitud_det',
					root: 'datos',
					totalProperty: 'total',
					fields: ['id_solicitud_det','id_centro_costo','descripcion', 'precio_unitario',
					         'id_solicitud','id_orden_trabajo','id_concepto_ingas','precio_total','cantidad_sol',
							 'desc_centro_costo','desc_concepto_ingas','desc_orden_trabajo'
					],remoteSort: true,
					baseParams: {dir:'ASC',sort:'id_solicitud_det',limit:'50',start:'0'}
				});
    	
    	this.editorDetail = new Ext.ux.grid.RowEditor({
                saveText: 'Aceptar',
                name: 'btn_editor'
               
            });
            
        this.summary = new Ext.ux.grid.GridSummary();
        // al iniciar la edicion
        this.editorDetail.on('beforeedit', this.onInitAdd , this);
        
        //al cancelar la edicion
        this.editorDetail.on('canceledit', this.onCancelAdd , this);
        
        //al cancelar la edicion
        this.editorDetail.on('validateedit', this.onUpdateRegister, this);
        
        this.editorDetail.on('afteredit', this.onAfterEdit, this);
        
        
        
        
        
        
        
        this.megrid = new Ext.grid.GridPanel({
        	        layout: 'fit',
                    store:  this.mestore,
                    region: 'center',
                    split: true,
                    border: false,
                    plain: true,
                    //autoHeight: true,
                    plugins: [ this.editorDetail, this.summary ],
                    stripeRows: true,
                    tbar: [{
                        /*iconCls: 'badd',*/
                        text: '<i class="fa fa-plus-circle fa-lg"></i> Agregar Concepto',
                        scope: this,
                        width: '100',
                        handler: function(){
                        	if(this.evaluaRequistos() === true){
                        		
	                        		 var e = new Items({
	                        		 	id_producto: undefined,
		                                cantidad_pro: 1,
		                                precio_afiliado:0,
		                                puntos_volumen:0,
		                                puntos_comisionables:0,
										 total_puntos:0,
		                                precio_total: 0
	                            });
	                            this.editorDetail.stopEditing();
	                            this.mestore.insert(0, e);
	                            this.megrid.getView().refresh();
	                            this.megrid.getSelectionModel().selectRow(0);
	                            this.editorDetail.startEditing(0);
	                            this.sw_init_add = true;
	                            
	                            this.bloqueaRequisitos(true);
                        	}
                        	else{
                        		//alert('Verifique los requisitos');
                        	}
                           
                        }
                    },{
                        ref: '../removeBtn',
                        text: '<i class="fa fa-trash fa-lg"></i> Eliminar',
                        scope:this,
                        handler: function(){
                            this.editorDetail.stopEditing();
                            var s = this.megrid.getSelectionModel().getSelections();
                            for(var i = 0, r; r = s[i]; i++){
                                this.mestore.remove(r);
                            }
                            this.evaluaGrilla();
                        }
                    }],
            
                    columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'Producto',
                        
                        dataIndex: 'id_producto',
                        width: 200,
                        sortable: false,
                        renderer:function(value, p, record){return String.format('{0}', record.data['codigo']);},
                        editor: this.detCmp.id_producto 
                    },
                   
                    {
                       
                        header: 'puntos_volumen',
                        dataIndex: 'puntos_volumen',
                        align: 'center',
                        width: 200,
                        editor: this.detCmp.puntos_volumen 
                    },
                    {
                       
                        header: 'puntos_comisionables',
                        dataIndex: 'puntos_comisionables',
                        align: 'center',
                        width: 200,
                        editor: this.detCmp.puntos_comisionables 
                    },
                    
                     {
                       
                        header: 'precio_afiliado',
                        dataIndex: 'precio_afiliado',
                        align: 'center',
                        width: 200,
                        editor: this.detCmp.precio_afiliado 
                    },

						{
							xtype: 'numbercolumn',
							header: 'Total P',
							dataIndex: 'total_puntos',
							format: '$0,0.00',
							width: 50,
							sortable: false,
							summaryType: 'sum',
							editor: this.detCmp.total_puntos
						},

                    {
                       
                        header: 'Cantidad',
                        dataIndex: 'cantidad_pro',
                        align: 'center',
                        width: 50,
                        summaryType: 'sum',
                        editor: this.detCmp.cantidad_pro 
                    },
                    
                    
                  
                    {
                        xtype: 'numbercolumn',
                        header: 'Importe Total',
                        dataIndex: 'precio_total',
                        format: '$0,0.00',
                        width: 50,
                        sortable: false,
                        summaryType: 'sum',
                        editor: this.detCmp.precio_total 
                    }]
                });
    },
    buildGrupos: function(){
    	this.Grupos = [{
    	           	    layout: 'border',
    	           	    border: false,
    	           	     frame:true,
	                    items:[
	                      {
                        	xtype: 'fieldset',
	                        border: false,
	                        split: true,
	                        layout: 'column',
	                        region: 'north',
	                        autoScroll: true,
	                        autoHeight: true,
	                        collapseFirst : false,
	                        collapsible: true,
	                        width: '100%',
	                        //autoHeight: true,
	                        padding: '0 0 0 10',
	    	                items:[
		    	                   {
							        bodyStyle: 'padding-right:5px;',
							       
							        autoHeight: true,
							        border: false,
							        items:[
			    	                   {
			                            xtype: 'fieldset',
			                            frame: true,
			                            border: false,
			                            layout: 'form',	
			                            title: 'Tipo',
			                            width: '33%',
			                            
			                            //margins: '0 0 0 5',
			                            padding: '0 0 0 10',
			                            bodyStyle: 'padding-left:5px;',
			                            id_grupo: 0,
			                            items: [],
			                         }]
			                     },
			                     {
			                      bodyStyle: 'padding-right:5px;',
			                    
			                      border: false,
			                      autoHeight: true,
							      items: [{
			                            xtype: 'fieldset',
			                            frame: true,
			                            layout: 'form',
			                            title: ' Datos básicos ',
			                            width: '33%',
			                            border: false,
			                            //margins: '0 0 0 5',
			                            padding: '0 0 0 10',
			                            bodyStyle: 'padding-left:5px;',
			                            id_grupo: 1,
			                            items: [],
			                         }]
		                         },
			                     {
			                      bodyStyle: 'padding-right:2px;',
			                     
			                      border: false,
			                      autoHeight: true,
							      items: [{
			                            xtype: 'fieldset',
			                            frame: true,
			                            layout: 'form',
			                            title: 'Tiempo',
			                            width: '33%',
			                            border: false,
			                            //margins: '0 0 0 5',
			                            padding: '0 0 0 10',
			                            bodyStyle: 'padding-left:2px;',
			                            id_grupo: 2,
			                            items: [],
			                         }]
		                         }
    	                      ]
    	                  },
    	                    this.megrid
                         ]
                 }];
    	
    	
    },
    
    loadValoresIniciales:function() 
    {        
        
       Phx.vista.formVenta.superclass.loadValoresIniciales.call(this);
        
        
        
    },
    
    successSave:function(resp)
    {
        Phx.CP.loadingHide();
        Phx.CP.getPagina(this.idContenedorPadre).reload();
        this.panel.close();
    },

	successExport: function (resp) {
		//Phx.CP.loadingHide();
		console.log(resp)
		this.megrid.store.removeAll();
		this.resetGroup(1);
		this.resetGroup(2);
		this.resetGroup(3);
		//doc.write(texto);
		var objRes = Ext.util.JSON.decode(Ext.util.Format.trim(resp.responseText));

		console.log(objRes)
		//console.log(objRes.ROOT.datos[0].length)
		var i = 0;
		objRes.datos.forEach(function (item) {
			var texto = item;
			ifrm = document.createElement("IFRAME");
			ifrm.name = 'mifr' + i;
			ifrm.id = 'mifr' + i;
			document.body.appendChild(ifrm);
			var doc = window.frames['mifr' + i].document;
			doc.open();
			doc.write(texto);
			doc.close();
			i++;
		});
		//this.Cmp.liquidevolu.disable();
		//this.Cmp.liquidevolu.setValue('');
		//this.Cmp.boletos_id.disable();
		//this.Cmp.id_factura.disable();
		//this.megrid.disable();

	},
                
    
   
	Atributos:[
		/*{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_solicitud'
			},
			type:'Field',
			form:true 
		},
		*/
		{
			config: {
				name: 'id_afiliado',
				hiddenName: 'id_afiliado',
				fieldLabel: 'Afiliado',
				typeAhead: false,
				forceSelection: false,
				allowBlank: false,
				emptyText: 'afiliados...',
				store: new Ext.data.JsonStore({
					url: '../../sis_afiliacion/control/Afiliado/listarAfiliado',
					id: 'id_afiliado',
					root: 'datos',
					sortInfo: {
						field: 'codigo',
						direction: 'ASC'

					},
					totalProperty: 'total',
					fields: ['id_afiliado', 'desc_person', 'direccion_principal', 'codigo','ci'],
					remoteSort: true,
					baseParams: {par_filtro: 'afil.direccion_principal#afil.codigo'}
				}),
				
				
								
				valueField: 'id_afiliado',
				displayField: 'codigo',
				triggerAction: 'all',
				lazyRender: true,
				mode: 'remote',
				pageSize: 20,
				queryDelay: 200,
				listWidth:280,
				minChars: 2,
				width: '80%',
				tpl:'<tpl for="."><div class="x-combo-list-item"><p>{desc_person}</p><p>CI:{ci}</p>CODIGO: {codigo} </div></tpl>',
			},
			type: 'ComboBox',
			id_grupo: 0,
			form: true
		},

		{
			config: {
				name: 'id_sucursal',
				hiddenName: 'id_sucursal',
				fieldLabel: 'Sucursal',
				typeAhead: false,
				forceSelection: false,
				allowBlank: false,
				emptyText: 'Sucuesales...',
				store: new Ext.data.JsonStore({
					url: '../../sis_sucursal/control/EmpleadoSucursal/listarEmpleadoSucursal',
					id: 'id_empleado_sucursal',
					root: 'datos',
					sortInfo: {
						field: 'id_empleado_sucursal',
						direction: 'ASC'

					},
					totalProperty: 'total',
					fields: ['id_empleado_sucursal', 'id_sucursal', 'desc_person', 'nombre_sucursal'],
					remoteSort: true,
					baseParams: {empleado_sucursal : 'si', par_filtro: 'emsu.nombre_sucursal#emsu.nombre_sucursal'}
				}),



				valueField: 'id_sucursal',
				displayField: 'nombre_sucursal',
				triggerAction: 'all',
				lazyRender: true,
				mode: 'remote',
				pageSize: 20,
				queryDelay: 200,
				listWidth:280,
				minChars: 2,
				width: '80%',
				tpl:'<tpl for="."><div class="x-combo-list-item"><p>{desc_person}</p><p>SUC:{nombre_sucursal}</p></div></tpl>',
			},
			type: 'ComboBox',
			id_grupo: 1,
			form: true
		},





		{
            config:{
                name: 'fecha',
                fieldLabel: 'Fecha',
                allowBlank: false,
                disabled: false,
                width: 105,
                format: 'd/m/Y'
            },
            type: 'DateField',
            id_grupo: 2,
            form: true
        },	     
		
      
     
		
	    
	],
	title: 'Frm solicitud',
	
	iniciarEventos:function(){
        
        this.cmpFechaSoli = this.getComponente('fecha');
         
        //inicio de eventos 
         
      
    },
    

    successGestion:function(resp){
       Phx.CP.loadingHide();
            var reg = Ext.util.JSON.decode(Ext.util.Format.trim(resp.responseText));
            if(!reg.ROOT.error){
                
                this.cmpIdGestion.setValue(reg.ROOT.datos.id_gestion);
                
               
            }else{
                
                alert('ocurrio al obtener la gestion')
            } 
    },
    onEdit:function(){
       this.cmpFechaSoli.disable();



    },
    
    onNew: function(){
    	
        this.form.getForm().reset();
        this.loadValoresIniciales();
        if(this.getValidComponente(0)){
        	this.getValidComponente(0).focus(false, 100);
        }

        this.Cmp.fecha.enable();
        this.Cmp.fecha.setValue(new Date());
        this.Cmp.fecha.fireEvent('change');

       
       
      /* this.Cmp.id_categoria_compra.store.load({params:{start:0,limit:this.tam_pag},
	       callback : function (r) {
	       		if (r.length == 1 ) {	       				
	    			this.Cmp.id_categoria_compra.setValue(r[0].data.id_categoria_compra);
	    		}    
	    			    		
	    	}, scope : this
	    });*/
	    
	    
	  /*  this.Cmp.id_depto.store.load({params:{start:0,limit:this.tam_pag},
           callback : function (r) {
                if (r.length == 1 ) {                       
                    this.Cmp.id_depto.setValue(r[0].data.id_depto);
                }    
                                
            }, scope : this
        });*/
	    
	    
	  /*  this.Cmp.id_funcionario.store.load({params:{start:0,limit:this.tam_pag},
	       callback : function (r) {
	       		if (r.length == 1 ) {	       				
	    			this.Cmp.id_funcionario.setValue(r[0].data.id_funcionario);
	    			this.Cmp.id_funcionario.fireEvent('select', r[0]);
	    		}    
	    			    		
	    	}, scope : this
	    });*/
	    
	    
		
           
    },
   
    onSubmit: function(o) {



    	//  validar formularios
        var arra = [], i, me = this;
        for (i = 0; i < me.megrid.store.getCount(); i++) {
    		record = me.megrid.store.getAt(i);
    		arra[i] = record.data;
    		arra[i].precio_ga = record.data.precio_total;
    		arra[i].precio_sg = 0.0; 
		}
   	    me.argumentExtraSubmit = { 'json_new_records': Ext.encode(arra) };
   	    if( i > 0){

   	    	Phx.vista.formVenta.superclass.onSubmit.call(this,o);
		}
		else{
			alert('no tiene ningun concepto  para comprar');
   	    }
   	},
   
   successSave: function(resp){

	   console.log(resp);
   	
      	Phx.CP.loadingHide();
   	    var objRes = Ext.util.JSON.decode(Ext.util.Format.trim(resp.responseText));
   	    this.fireEvent('successsave',this,objRes);

	   console.log(objRes.ROOT.datos.id_ficha)



	   Ext.Ajax.request({
		   url: '../../sis_almacen/control/Ficha/generarFicha',
		   params: {'id_ficha': objRes.ROOT.datos.id_ficha,'start':'0','limit':'50','sort':'id_ficha','dir':'ASC'},
		   success: this.successExport,
		   failure: this.conexionFailure,
		   timeout: this.timeout,
		   scope: this
	   });


	   console.log('llega todo')
   	    
   	},
	
	loadCheckDocumentosSolWf:function(data) {
		   //TODO Eventos para cuando ce cierre o destruye la interface de documentos
            Phx.CP.loadWindows('../../../sis_workflow/vista/documento_wf/DocumentoWf.php',
                    'Documentos del Proceso',
                    {
                        width:'90%',
                        height:500
                    },
                    data,
                    this.idContenedor,
                   'DocumentoWf'
       );
       
    }
	
    
})    
</script>